<?php
    
    //First we declare this variable to be a integer
    $number = 33;

    //note there is html code in the quotes, because PHP code in an echo statement is converted into HTML code.
    
    echo $number."<br>"; 

    //Then we change it into a string
    $number = "Thirty-Three";

    echo $number;
?>